#!/bin/bash
# -------------------------------------
# @edt ASIX-M01 Curs 2022/2023
# Març 2023
# Isaac Gordillo
# -------------------------------------
#
# Descripción:
#
# Comprobar el tipo de archivo del argumento.
#
#
# Especificaciones d'entrada:
#
# 1 int vía argumento posicional.

# 0. Declaración de los tipos de error.

ERR_ARGS=1

# 1. Validar que existe solo un argumento.

if [ $# -ne 1 ]
then
  echo "ERROR: Número de argumentos incorrecto.\n
        Cantidad de argumentos introducida: $#"
  echo "Usage: $0 edad"
  exit $ERR_ARGS
fi

# 2. El programa:

archivo=$1
if [ ! -e $archivo ]; then
  echo "$archivo no existe."
  exit $ERR_NOEXISTE
elif [ -f $archivo ]; then
  echo "$archivo ste."
  exit $ERR_NOEXISTE