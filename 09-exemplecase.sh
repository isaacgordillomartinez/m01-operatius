#!/bin/bash
# -------------------------------------
# @edt ASIX-M01 Curs 2022/2023
# Març 2023
# Isaac Gordillo
# -------------------------------------
#
# Descripción:
#
# Comprobar el tipo de archivo del argumento.
#
#
# Especificaciones d'entrada:
#
# 1 int vía argumento posicional.

# 0. Declaración de los tipos de error.

ERR_ARGS=1

# 1. Validar que existe solo un argumento.

if [ $# -ne 1 ]
then
  echo "ERROR: Número de argumentos incorrecto.\n
        Cantidad de argumentos introducida: $#"
  echo "Usage: $0 edad"
  exit $ERR_ARGS
fi

# Sintaxis:

# case algo in
#   PER1)
#   acción1
#   ;;
#   PER2
#   acción2
#   ;;
#   *)
#   acción3
#   ;;
# esac

# ) equivale a elif y *) equivale a else.

# La diferencia que hay entre el if y el case es que en el if se
# usan condiciones y en el case se usan constantes, que son PERs
# y pueden estar o no entrecomillados

case $1 in
  "[aeiou]")
  echo "És una vocal"
  ;;
  "[^aeiou]")
  echo "És una consonant"
  ;;
  *)
  echo "És una altra cosa"
  ;;
esac

exit 0

case $1 in
  "pere"|"pau"|"joan")
  echo "És un nen"
  ;;
  "anna"|"joana"|"marta")
  echo "És una nena"
  ;;
  *)
  echo "No binari"
  ;;
esac
exit 0