#!/bin/bash
# -------------------------------------
# @edt ASIX-M01 Curs 2022/2023
# Març 2023
# Isaac Gordillo
# -------------------------------------
#
# Sintaxis:
#
# 11-exemple-for.sh num1 num2 num3 ...
#
#
# Descripción:
#
# Realizar la suma de los números introducidos.
#
#
# Especificaciones de entrada:
#
# n int vía argumento posicional.

# 0. Declaración de los tipos de error.

ERR_ARGS=1

# 1. Validar que existe solo un argumento.

if [ $# -ne 1 ]; then
  echo "ERROR: Número de argumentos incorrecto.\n
        Cantidad de argumentos introducida: $#"
  echo "Usage: $0 edad"
  exit $ERR_ARGS
fi

# 2. Suma.

suma = 0

case $1 in
  "pere"|"pau"|"joan")
  echo "És un nen"
  ;;
  "anna"|"joana"|"marta")
  echo "És una nena"
  ;;
  *)
  echo "No binari"
  ;;
esac
exit 0