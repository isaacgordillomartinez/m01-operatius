#!/bin/bash
# -------------------------------------
# @edt ASIX-M01 Curs 2022/2023
# Març 2023
# Isaac Gordillo
# -------------------------------------
#
# Sintaxis:
#
# 14a-llista-dir.sh
#
#
# Descripción:
#
# Comprobar que hay como mínimo un argumento y que son directorios.
# Tras esto, listar sus contenidos numerando las líneas del resultado.
#
#
# Especificaciones de entrada:
#
# n dirs.

# IMPORTANTE: SOLO FUNCIONA CON . COMO ARGUMENTO (REPASAR)

# 0. Declaración de los tipos de error.

ERR_ARGS=1
ERR_DIR=0

# 1. Validar que existe, como mínimo, un argumento.

  # Pese a que matemáticamente es correcto comprobar que la
  # cantidad de argumentos está entre (n y 1), es decir, -lt 1,
  # no se puede introducir una cantidad inferior a 0.

if [ $# -eq 0 ]; then
  echo -e "ERROR: Hay que introducir mínimo un argumento.\n\
        Cantidad de argumentos introducida: $#"
  echo "Usage: $0 dir"
  exit $ERR_ARGS
fi

# 2. Validar que los argumentos son directorios.

for dir in $*
do
  if ! [ -d $1 ]; then
    echo -e "ERROR: El argumento introducido tiene que ser un directorio.\n\
          Tipo de argumento introducido: $(file $1 >/dev/null)" >> /dev/stderr
    echo "Usage: $0 dir" >> /dev/stderr
    ERR_DIR=2
  else

# 3. Programa: listar de forma numerada el contenido del directorio.

    lista_contenido=$(ls $1)
    contador=0

    for file in $lista_contenido
    do
      ((contador++))
      if [ -L $file ]; then
        echo "$contador: $file es un link"
      elif [ -d $file ]; then
        echo "$contador: $file es un directorio"
      elif [ -f $file ]; then
        echo "$contador: $file es un fichero"
      fi
    done
  fi
done
exit $ERR_DIR