#!/bin/bash
# -------------------------------------
# @edt ASIX-M01 Curs 2022/2023
# Febrer 2023
# Isaac Gordillo
# -------------------------------------
#
# Descripción:
#
# Programa de ejemplo
#
#
# Especificaciones d'entrada:
#
# Nada.

# Maneras de ejecutar el programa:
#
#   Al ejecutarlo de estas dos formas se inicia un shell nuevo,
#   por lo que $SHLVL = 2.
#
#       - bash /ruta/01-exemple.sh
#       - /ruta/01-exemple.sh --> Requiere chmod +x
#
#   Estas dos maneras lo ejecutan en el mismo SHLVL.
#
#       - source /ruta/01-exemple.sh
#       - . /ruta/01-exemple.sh --> Este punto equivale a source.

echo "Hola"

nom="pere pou prat"
edat=""