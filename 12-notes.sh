#!/bin/bash
# -------------------------------------
# @edt ASIX-M01 Curs 2022/2023
# Març 2023
# Isaac Gordillo
# -------------------------------------
#
# Sintaxis:
#
# 12-notes.sh nota1 nota2 nota3 ... notan
#
#
# Descripción:
#
# Comprobar si las notas introducidas son válidas y decir a qué equivalen
# (suspenso, excelente, etc.).
#
#
# Especificaciones de entrada:
#
# n int vía argumento posicional.

# 0. Declaración de los tipos de error.

ERR_NOTA=0
ERR_ARGS=1

# 1. Validar que existe solo un argumento.

if [ $# -lt 1 ]; then
  echo "ERROR: Número de argumentos incorrecto.\n
        Cantidad de argumentos introducida: $#"
  echo "Usage: $0 nota1 nota2 nota3 ... notan"
  exit $ERR_ARGS
fi

# 2. Programa principal.

for nota in $*
do
  if ! [ $nota -ge 0 -a $nota -le 10 ]; then
    echo "ERROR: $nota no es una nota correcta (tiene que ser un número del 0 al 10)." >> /dev/stderr
    ERR_NOTA=2

  elif [ $nota -lt 5 ]; then
    echo -e "\t $nota equivale a un suspenso."
  elif [ $nota -lt 7 ]; then
    echo -e "\t $nota equivale a un bien."
  elif [ $nota -lt 9 ]; then
    echo -e "\t $nota equivale a un notable."
  else
    echo -e "\t $nota equivale a un excelente."
  fi
done

exit $ERR_NOTA