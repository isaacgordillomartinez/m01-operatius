#!/bin/bash
# -------------------------------------
# @edt ASIX-M01 Curs 2022/2023
# Març 2023
# Isaac Gordillo
# -------------------------------------
#
# Sintaxis:
#
# 13-exemples-while.sh
#
#
# Descripción:
#
# Ejemplos de uso del bucle while.
#
#
# Especificaciones de entrada:
#
# n args de cualquier tipo.

# 0. Declaración de los tipos de error.

ERR_ARGS=1

# 4. Programa 3: mostrar stdin línea a línea numerada y en mayúsculas.

num=1

while read -r line
do
  echo "$num: $line" | tr '[:lower:]' '[:upper:]'
  ((num++))
done
exit 0

# 3. Programa 2: mostrar uso de la función shift.

while [ -n "$1" ] # $1 tiene que estar encapsulado.
do
  echo "$1"
  # Esta función descarta el parámetro $1 después de ser usado.
  # $0 es usado nunca por shift.
  shift
done
exit 0

# 2. Programa 1: mostrar un contador.

MAX=$1
num=0

while [ $num -le $MAX ]
do
  echo "$num"
  ((num++))
done
exit 0

# 1. Validar que existe solo un argumento.

if [ $# -ne 1 ]; then
  echo "ERROR: Número de argumentos incorrecto.\n
        Cantidad de argumentos introducida: $#"
  echo "Usage: $0 nota1 nota2 nota3 ... notan"
  exit $ERR_ARGS
fi