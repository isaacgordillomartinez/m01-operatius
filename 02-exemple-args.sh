#!/bin/bash
# -------------------------------------
# @edt ASIX-M01 Curs 2022/2023
# Febrer 2023
# Isaac Gordillo
# -------------------------------------
#
# Descripción:
#
# Ejemplo de procesamiento de argumentos.
#
#
# Especificaciones d'entrada:
#
# 1.

# Hay que ejecutar el programa así:
# bash 02-exemple-args.sh arg1 arg2 arg3 arg4 arg5 arg6 arg7 arg8 arg9 arg10

echo "\$*: $*"
echo "\$@: $@"
echo "\$#: $#"

echo "\$?: $?"
echo "\$?: $$"

echo "\$0: $0"
echo "\$1: $1"
echo "\$2: $2"

echo "\$10: ${10}"