#!/bin/bash
# -------------------------------------
# @edt ASIX-M01 Curs 2022/2023
# Febrer 2023
# Isaac Gordillo
# -------------------------------------
#
# Descripción:
#
# Ejemplo de estructura condicional básica.
#
#
# Especificaciones d'entrada:
#
# 1 int vía argumento posicional.

# 0. Declaración de los tipos de error.

ERR_ARGS=1

# 1. Validar que existe solo un argumento.

if [ $# -ne 1 ]
then
  echo "ERROR: Número de argumentos incorrecto.\n
        Cantidad de argumentos introducida: $#"
  echo "Usage: $0 edad"
  exit $ERR_ARGS
fi

# 2. El programa en sí. En este caso hay que validar si un número
#    representa o no la mayoría de edad.

edad=$1
if [ $edad -ge 18 ]
then
  echo "La edad introducida ($1) representa la mayoría de edad."
else
  echo "La edad introducida ($1) NO representa la mayoría de edad."
fi
exit 0